#include "Define.h"
#include "GossipDef.h"
#include "Item.h"
#include "Player.h"
#include "ScriptedGossip.h"
#include "ScriptMgr.h"
#include "Spell.h"
#include "Config.h"
#include "CharacterDatabase.h"
#include "LoginDatabase.h"
#include "DatabaseWorkerPool.h"
#include "MySQLConnection.h"
#include "Chat.h"
#include "Language.h"

class premium_account_login : public PlayerScript
{
public:
    premium_account_login() : PlayerScript("premium_account_login") {}

    // RewardTimer System
    uint32    initialTimer = (sConfigMgr->GetIntDefault("RewardTime", 1) * HOUR * IN_MILLISECONDS);
    uint32    RewardTimer = initialTimer;

    // Fake palyer
    uint32   initialTimerFake = (sConfigMgr->GetIntDefault("FakePlayerChangeZoneTime", 1) * MINUTE * IN_MILLISECONDS);
    uint32    ChangeTimerFakeUpadate = initialTimerFake;


    void OnLogin(Player* player) override
    {
        if (player->GetSession()->IsPremium())
        {
            uint32 accountID = player->GetSession()->GetAccountId();
            int64 setTime = -1;
            QueryResult result = LoginDatabase.PQuery("SELECT unsetdate FROM account_premium WHERE id = %i AND active ORDER BY setdate ASC LIMIT 1", accountID);
            Field* fields = result->Fetch();
            setTime =  fields[0].GetUInt32();
            ChatHandler(player->GetSession()).SendSysMessage("|cff4CFF00Account is Premium");
            ChatHandler(player->GetSession()).SendSysMessage(secsToTimeString(setTime - time(nullptr), true).c_str());
        }
    }

    void OnFirstLogin(Player* player)
    {

        if (sConfigMgr->GetBoolDefault("OnFirstLoginEnable", true))
        {
            uint32 OnFirstLoginItemId = sConfigMgr->GetIntDefault("OnFirstLoginItemId", 37829);
            uint32 OnFirstLoginItemCount = sConfigMgr->GetIntDefault("OnFirstLoginItemCount", 25);
            std::string subject = "Token First Login";
          //  SendRewardToPlayer(player, OnFirstLoginItemId, OnFirstLoginItemCount, subject);
             player->ModifyMoney(200000);
        }
    }

    void OnGiveXP(Player* player, uint32& amount, Unit* /*victim*/) override
    {
        if (sConfigMgr->GetBoolDefault("Dynamic.XP.Rate", true))

        {
            if (player->getLevel() <= 9)
                amount *= sConfigMgr->GetIntDefault("Dynamic.XP.Rate.1-9", 1);

            else if (player->getLevel() <= 19)
                amount *= sConfigMgr->GetIntDefault("Dynamic.XP.Rate.10-19", 2);

            else if (player->getLevel() <= 29)
                amount *= sConfigMgr->GetIntDefault("Dynamic.XP.Rate.20-29", 3);

            else if (player->getLevel() <= 39)
                amount *= sConfigMgr->GetIntDefault("Dynamic.XP.Rate.30-39", 4);

            else if (player->getLevel() <= 49)
                amount *= sConfigMgr->GetIntDefault("Dynamic.XP.Rate.40-49", 5);

            else if (player->getLevel() <= 59)
                amount *= sConfigMgr->GetIntDefault("Dynamic.XP.Rate.50-59", 6);

            else if (player->getLevel() <= 69)
                amount *= sConfigMgr->GetIntDefault("Dynamic.XP.Rate.60-69", 7);

            else if (player->getLevel() <= 79)
                amount *= sConfigMgr->GetIntDefault("Dynamic.XP.Rate.70-79", 8);
        }
    }


    void OnBeforeUpdate(Player* player, uint32 p_time) override
    {


        if (sWorld->getBoolConfig(CONFIG_FAKE_WHO_LIST))
        {
            if (ChangeTimerFakeUpadate > 0)
            {
                if (ChangeTimerFakeUpadate <= p_time)
                {
                    CharacterDatabase.PExecute("UPDATE characters_fake SET zone = (FLOOR(800 * RAND()) + 1) WHERE online = 1");
                    CharacterDatabase.PExecute("UPDATE characters_fake SET level = level+1 WHERE level < 80 AND online = 1");
                    ChangeTimerFakeUpadate = initialTimerFake;
                }
                else
                    ChangeTimerFakeUpadate -= p_time;
            }
        }


        if (sConfigMgr->GetBoolDefault("RewardSystemEnable", true))
        {
            uint32    ptr_Money       = sConfigMgr->GetIntDefault("PlayedTimeReward.Money", 100000);
            uint32    ptr_Honor       = sConfigMgr->GetIntDefault("PlayedTimeReward.Honor", 1);
            uint32    ptr_Arena       = sConfigMgr->GetIntDefault("PlayedTimeReward.Arena", 1);
            uint32    RewardItemID    = sConfigMgr->GetIntDefault("RewardItemID", 1);
            uint32    RewardItemCount = sConfigMgr->GetIntDefault("RewardItemCount", 1);
            std::string subject = "Play Time System ";

            if (RewardTimer > 0)
            {

                if (RewardTimer <= p_time)
                {
                   
                        SendRewardToPlayer(player, RewardItemID, RewardItemCount, subject);
                        player->ModifyMoney(ptr_Money);
                        player->ModifyHonorPoints(ptr_Honor);
                        player->ModifyArenaPoints(ptr_Arena);               
                        RewardTimer = initialTimer;
                }
                else  
                {
                    RewardTimer -= p_time;
                   // ChatHandler(player->GetSession()).PSendSysMessage("Before time %u RewardTimer %u", p_time, RewardTimer);
                }
                
            }
        }
    }


    void SendRewardToPlayer(Player* receiver, uint32 itemId, uint32 count, std::string subject)
    {
        if (receiver->IsInWorld() && receiver->AddItem(itemId, count))
            return;

        ChatHandler(receiver->GetSession()).PSendSysMessage("You will receive your item in your mailbox");
        // format: name "subject text" "mail text" item1[:count1] item2[:count2] ... item12[:count12]
        uint64 receiverGuid = receiver->GetGUID().GetCounter();
        std::string receiverName;
        std::string text = "Congratulations, you won a prize but your inventory was full. Please take your items when you will free space from your inventory";

        ItemTemplate const* item_proto = sObjectMgr->GetItemTemplate(itemId);

        if (!item_proto)
        {
            sLog->outError("The itemId is invalid: %u", itemId);
            return;
        }

        if (count < 1 || (item_proto->MaxCount > 0 && count > uint32(item_proto->MaxCount)))
        {
            sLog->outError(" The item count is invalid: %u : %u", itemId, count);
            return;
        }

        typedef std::pair<uint32, uint32> ItemPair;
        typedef std::list< ItemPair > ItemPairs;
        ItemPairs items;

        while (count > item_proto->GetMaxStackSize())
        {
            items.push_back(ItemPair(itemId, item_proto->GetMaxStackSize()));
            count -= item_proto->GetMaxStackSize();
        }

        items.push_back(ItemPair(itemId, count));

        if (items.size() > MAX_MAIL_ITEMS)
        {
            sLog->outError(" Maximum email items is %u, current size: %lu", MAX_MAIL_ITEMS, items.size());
            return;
        }

        // from console show not existed sender
        MailSender sender(MAIL_NORMAL, receiver->GetSession() ? receiver->GetGUID().GetCounter() : 0, MAIL_STATIONERY_TEST);

        // fill mail
        MailDraft draft(subject, text);

        SQLTransaction trans = CharacterDatabase.BeginTransaction();

        for (ItemPairs::const_iterator itr = items.begin(); itr != items.end(); ++itr)
        {
            if (Item* item = Item::CreateItem(itr->first, itr->second, receiver->GetSession() ? receiver : 0))
            {
                item->SaveToDB(trans);                               // save for prevent lost at next mail load, if send fail then item will deleted
                draft.AddItem(item);
            }
        }

        draft.SendMailTo(trans, MailReceiver(receiver, receiverGuid), sender);
        CharacterDatabase.CommitTransaction(trans);

        return;
    }




    void OnLevelChanged(Player* player, uint8 oldLevel)
    {
        uint32  OnLevelChangedMoney = sConfigMgr->GetIntDefault("OnLevelChangedMoney", 2000000);
        uint32  MaxLevelPlayer = sConfigMgr->GetIntDefault("MaxLevelPlayer", 80);

        if (sConfigMgr->GetBoolDefault("OnLevelChangedEnable", true))
        {
            if (player->getLevel() == MaxLevelPlayer)
            {
                player->ModifyMoney(OnLevelChangedMoney);

                if (sConfigMgr->GetBoolDefault("OnLevelChangedSendItemEnable", true))
                {
                    uint32 OnLevelChangedItemId = sConfigMgr->GetIntDefault("OnLevelChangedItemId", 37829);
                    uint32 OnLevelChangedItemCount = sConfigMgr->GetIntDefault("OnLevelChangedItemCount", 5);
                  
                    std::string subject = "Token max Level";
                    SendRewardToPlayer(player, OnLevelChangedItemId, OnLevelChangedItemCount, subject);
                }
            }

        }
    }

};



void AddSC_premium_account()
{
   
    new premium_account_login();
    
}
