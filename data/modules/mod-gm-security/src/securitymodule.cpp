#include "Channel.h"
#include "ScriptMgr.h"
#include "Player.h"

enum ForbiddenAreas
{
	AREA_GM_ISLAND           = 876,   // GM Island
    AREA_TESTING             = 3817,   // TESTING
};

class map_security : public PlayerScript
{
public:
	map_security() : PlayerScript("map_security") {}

    void OnUpdateZone(Player* player, uint32 /*newZone */, uint32 /*newArea*/)
	{
		// Forbidden areas:
		switch (player->GetAreaId())
		{
		case AREA_GM_ISLAND:
        case AREA_TESTING:
			{
				if (player->GetSession()->GetSecurity() >= 1)
					return;

                player->TeleportTo(1, 16218.700195, 16403.599609, -64.378288, 6.263502); // Prison
 
                player->GetSession()->SendAreaTriggerMessage("Sorry but you are not a GameMaster this area is off limits.");
			}
        
           
			break;
		}
	}
};

class gamemasters_security : public PlayerScript
{
public:
	gamemasters_security() : PlayerScript("gamemasters_security") {}

	void OnLogin(Player* player)
	{

        // Prevent GMs at all ranks to play as a normal player
        if (player->GetSession()->GetSecurity() == 1 || player->GetSession()->GetSecurity() == 2 || player->GetSession()->GetSecurity() == 3)
        {
            for (uint8 slot = EQUIPMENT_SLOT_START; slot < EQUIPMENT_SLOT_END; slot++)
                player->DestroyItem(INVENTORY_SLOT_BAG_0, slot, true);
            for (uint8 slot = INVENTORY_SLOT_BAG_START; slot < INVENTORY_SLOT_BAG_END; slot++)
                player->DestroyItem(INVENTORY_SLOT_BAG_0, slot, true);
            for (uint8 slot = INVENTORY_SLOT_ITEM_START; slot < INVENTORY_SLOT_ITEM_END; slot++)
                player->DestroyItem(INVENTORY_SLOT_BAG_0, slot, true);
            for (uint8 slot = BANK_SLOT_ITEM_START; slot < BANK_SLOT_ITEM_END; slot++)
                player->DestroyItem(INVENTORY_SLOT_BAG_0, slot, true);
            for (uint8 slot = BANK_SLOT_BAG_START; slot < BANK_SLOT_BAG_END; slot++)
                player->DestroyItem(INVENTORY_SLOT_BAG_0, slot, true);
            for (uint8 slot = BUYBACK_SLOT_START; slot < BUYBACK_SLOT_END; slot++)
                player->DestroyItem(INVENTORY_SLOT_BAG_0, slot, true);
 
            player->EquipNewItem(EQUIPMENT_SLOT_CHEST, 2586, true);
            player->EquipNewItem(EQUIPMENT_SLOT_FEET, 11508, true);
            player->EquipNewItem(EQUIPMENT_SLOT_HEAD, 12064, true);
        }
	}
};

void AddsecuritymoduleScripts()
{
    new map_security();
    new gamemasters_security();
}
