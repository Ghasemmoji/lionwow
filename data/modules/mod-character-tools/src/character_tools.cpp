#include "Define.h"
#include "GossipDef.h"
#include "Item.h"
#include "Player.h"
#include "ScriptedGossip.h"
#include "ScriptMgr.h"
#include "Spell.h"
#include "Configuration/Config.h"
#include "Chat.h"
#include "CharacterDatabase.h"
#include "LoginDatabase.h"
#include "DatabaseWorkerPool.h"
#include "MySQLConnection.h"
#include "Chat.h"
#include "Language.h"

enum Vendors
{
    NPC_VENDOR_A = 54,
    NPC_VENDOR_H = 3163,
    NPC_AUCTION_H = 9856,
    NPC_AUCTION_A = 8670
};

enum Trainers
{
    // Alliance
    DRUID_A = 5504,
    HUNTER_A = 5515,
    MAGE_A = 5497,
    PALADIN_A = 928,
    PRIEST_A = 376,
    ROGUE_A = 918,
    SHAMAN_A = 20407,
    WARLOCK_A = 461,
    WARRIOR_A = 5479,

    // Horde
    DRUID_H = 3033,
    HUNTER_H = 3406,
    MAGE_H = 5883,
    PALADIN_H = 23128,
    PRIEST_H = 3045,
    ROGUE_H = 3401,
    SHAMAN_H = 3344,
    WARLOCK_H = 3324,
    WARRIOR_H = 3354,

    DEATHKNIGHT_AH = 28472
};

enum Mounts
{
    HUMAN_MOUNT = 470,
    ORC_MOUNT = 6653,
    GNOME_MOUNT = 17454,
    NIGHTELF_MOUNT = 8394,
    DWARF_MOUNT = 6899,
    UNEAD_MOUNT = 17463,
    TAUREN_MOUNT = 64657,
    TROLL_MOUNT = 8395,
    BLOODELF_MOUNT = 35022,
    DRAENEI_MOUNT = 34406
};
class character_tools : public ItemScript
{
public:
    character_tools() : ItemScript("character_tools") {}

    bool OnUse(Player* p, Item* i, SpellCastTargets const& /*targets*/) override
    {
        

        if (p->IsInCombat())
            return false;

        if (!p->GetSession()->IsPremium())
            return false;

        if (!sConfigMgr->GetBoolDefault("CharacterTools", true))
            return false;

        p->PlayerTalkClass->ClearMenus();

        if (sConfigMgr->GetBoolDefault("Morph", true))
        {
            p->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Morph", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 1);
            p->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Demorph", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 2);
        }

        if (sConfigMgr->GetBoolDefault("Mount", true))
            p->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT_16, "Mount", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 6);

        if (sConfigMgr->GetBoolDefault("Trainers", true))
            p->ADD_GOSSIP_ITEM(GOSSIP_ICON_TRAINER, GOSSIP_TEXT_TRAIN, GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 8);

        if (sConfigMgr->GetBoolDefault("PlayerInteraction", true))
            p->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Player interactions", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 9);


        if (sConfigMgr->GetBoolDefault("TeleportDelaran", true))

            p->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Tele Delaran", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 10);
        if (sConfigMgr->GetBoolDefault("DuelZone", true))
            p->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "DuelZone", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 11);



        if (sConfigMgr->GetBoolDefault("ChangeMyRace", true))
            p->ADD_GOSSIP_ITEM(NULL, "|TInterface/Icons/Ability_Paladin_BeaconofLight:50:50|tChange My Race", GOSSIP_SENDER_MAIN, 12);

        if (sConfigMgr->GetBoolDefault("ChangeMyFaction", true))
        p->ADD_GOSSIP_ITEM(NULL, "|TInterface/Icons/INV_BannerPVP_01:50:50|tChange My Faction", GOSSIP_SENDER_MAIN, 13);

        if (sConfigMgr->GetBoolDefault("ChangeMyAppearance", true))
        p->ADD_GOSSIP_ITEM(NULL, "|TInterface/Icons/Achievement_BG_returnXflags_def_WSG:50:50|tChange My Appearance", GOSSIP_SENDER_MAIN, 14);

        if (sConfigMgr->GetBoolDefault("ChangeMyName", true))
        p->ADD_GOSSIP_ITEM(NULL, "|TInterface/Icons/INV_Inscription_Scroll:50:50|tChange My Name", GOSSIP_SENDER_MAIN, 15);


        p->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, i->GetGUID());

        return false; // If item has spell cast it normal.
    }

    void OnGossipSelect(Player* player, Item* item, uint32 /*sender*/, uint32 action) override
    {
        player->PlayerTalkClass->ClearMenus();
        switch (action)
        {

        case GOSSIP_ACTION_INFO_DEF + 1: /*Morph*/
        {
            player->CLOSE_GOSSIP_MENU();
            ApplyRandomMorph(player);
            break;
        }
        case GOSSIP_ACTION_INFO_DEF + 2: /*Demorph*/
        {
            player->DeMorph();
            player->CLOSE_GOSSIP_MENU();
            break;
        }
        case GOSSIP_ACTION_INFO_DEF + 3: /*Show Bank*/
        {
            player->GetSession()->SendShowBank(player->GetGUID());
            break;
        }
        case GOSSIP_ACTION_INFO_DEF + 4: /*Mail Box*/
        {
            player->GetSession()->SendShowMailBox(player->GetGUID());
            break;
        }
        case GOSSIP_ACTION_INFO_DEF + 5: /*Vendor*/
        {
            uint32 vendorId = 0;
            std::string salute;
            if (player->GetTeamId() == TEAM_ALLIANCE)
            {
                vendorId = NPC_VENDOR_A;
                salute = "Greetings";
            }
            else {
                vendorId = NPC_VENDOR_H;
                salute = "Zug zug";
            }

            SummonTempNPC(player, vendorId, salute.c_str());
            player->CLOSE_GOSSIP_MENU();
            break;
        }
        case GOSSIP_ACTION_INFO_DEF + 6: /*Mount*/
        {
            player->CLOSE_GOSSIP_MENU();
            switch (player->getRace())
            {
            case RACE_HUMAN:         player->CastSpell(player, HUMAN_MOUNT); break;
            case RACE_ORC:           player->CastSpell(player, ORC_MOUNT); break;
            case RACE_GNOME:         player->CastSpell(player, GNOME_MOUNT); break;
            case RACE_NIGHTELF:      player->CastSpell(player, NIGHTELF_MOUNT); break;
            case RACE_DWARF:         player->CastSpell(player, DWARF_MOUNT); break;
            case RACE_DRAENEI:       player->CastSpell(player, DRAENEI_MOUNT); break;
            case RACE_UNDEAD_PLAYER: player->CastSpell(player, UNEAD_MOUNT); break;
            case RACE_TAUREN:        player->CastSpell(player, TAUREN_MOUNT); break;
            case RACE_TROLL:         player->CastSpell(player, TROLL_MOUNT); break;
            case RACE_BLOODELF:      player->CastSpell(player, BLOODELF_MOUNT); break;
            }
            break;
        }
        case GOSSIP_ACTION_INFO_DEF + 7: /*Auction House*/
        {
            uint32 auctionId = 0;
            std::string salute;
            if (player->GetTeamId() == TEAM_HORDE)
            {
                auctionId = NPC_AUCTION_H;
                salute = "I will go shortly, I need to get back to Orgrimmar";
            }
            else
            {
                auctionId = NPC_AUCTION_A;
                salute = "I will go shortly, I need to get back to Stormwind City";
            }

            SummonTempNPC(player, auctionId, salute.c_str());
            player->CLOSE_GOSSIP_MENU();
            break;
        }
        case GOSSIP_ACTION_INFO_DEF + 8: /* Class Trainers*/
        {
            uint32 trainerId = 0;
            switch (player->getClass())
            {
            case CLASS_ROGUE:
                trainerId = player->GetTeamId() == TEAM_ALLIANCE ? ROGUE_A : ROGUE_H;
                break;
            case CLASS_WARRIOR:
                trainerId = player->GetTeamId() == TEAM_ALLIANCE ? WARRIOR_A : WARRIOR_H;
                break;
            case CLASS_PRIEST:
                trainerId = player->GetTeamId() == TEAM_ALLIANCE ? PRIEST_A : PRIEST_H;
                break;
            case CLASS_MAGE:
                trainerId = player->GetTeamId() == TEAM_ALLIANCE ? MAGE_A : MAGE_H;
                break;
            case CLASS_PALADIN:
                trainerId = player->GetTeamId() == TEAM_ALLIANCE ? PALADIN_A : PALADIN_H;
                break;
            case CLASS_HUNTER:
                trainerId = player->GetTeamId() == TEAM_ALLIANCE ? HUNTER_A : HUNTER_H;
                break;
            case CLASS_DRUID:
                trainerId = player->GetTeamId() == TEAM_ALLIANCE ? DRUID_A : DRUID_H;
                break;
            case CLASS_SHAMAN:
                trainerId = player->GetTeamId() == TEAM_ALLIANCE ? SHAMAN_A : SHAMAN_H;
                break;
            case CLASS_WARLOCK:
                trainerId = player->GetTeamId() == TEAM_ALLIANCE ? WARLOCK_A : WARLOCK_H;
                break;
            case CLASS_DEATH_KNIGHT:
                trainerId = DEATHKNIGHT_AH;
                break;
            }

            SummonTempNPC(player, trainerId);
            player->CLOSE_GOSSIP_MENU();
            break;
        }
        case GOSSIP_ACTION_INFO_DEF + 9: /*Player Interactions*/
        {
            player->PlayerTalkClass->ClearMenus();

            if (sConfigMgr->GetBoolDefault("Vendor", true))
                player->ADD_GOSSIP_ITEM(GOSSIP_ICON_VENDOR, "Vendor", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 5);

            if (sConfigMgr->GetBoolDefault("MailBox", true))
                player->ADD_GOSSIP_ITEM(GOSSIP_ICON_VENDOR, "Mail Box", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 4);

            if (sConfigMgr->GetBoolDefault("Bank", true))
                player->ADD_GOSSIP_ITEM(GOSSIP_ICON_VENDOR, "Show Bank", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 3);

            if (sConfigMgr->GetBoolDefault("Auction", true))
                player->ADD_GOSSIP_ITEM(GOSSIP_ICON_VENDOR, "Auction House", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 7);

            player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, item->GetGUID());
            break;
        }

        case 12:
            player->SetAtLoginFlag(AT_LOGIN_CHANGE_RACE);
            ChatHandler(player->GetSession()).PSendSysMessage("CHAT OUTPUT: Please log out for race change.");
            break;
        case 13:
            player->SetAtLoginFlag(AT_LOGIN_CHANGE_FACTION);
            ChatHandler(player->GetSession()).PSendSysMessage("CHAT OUTPUT: Please log out for faction change.");
            break;
        case 14:
            player->SetAtLoginFlag(AT_LOGIN_CUSTOMIZE);
            ChatHandler(player->GetSession()).PSendSysMessage("CHAT OUTPUT: Please log out for Character Customize.");
            break;
        case 15:
            player->SetAtLoginFlag(AT_LOGIN_RENAME);
            ChatHandler(player->GetSession()).PSendSysMessage("CHAT OUTPUT: Please log out for name change.");
            break;

        case 10:

            player->CLOSE_GOSSIP_MENU();

            if (player->IsInCombat())
            
                ChatHandler(player->GetSession()).PSendSysMessage("You can't use that right now!");
            else

            {
                QueryResult result = WorldDatabase.PQuery("SELECT `map`, `position_x`, `position_y`, `position_z`, `orientation` FROM game_tele WHERE name = 'Dalaran'");

                do
                {
                    Field* fields = result->Fetch();
                    uint32 map = fields[0].GetUInt32();
                    float position_x = fields[1].GetFloat();
                    float position_y = fields[2].GetFloat();
                    float position_z = fields[3].GetFloat();
                    float orientation = fields[4].GetFloat();

                    player->TeleportTo(map, position_x, position_y, position_z, orientation);
                } while (result->NextRow());


            }

            break;

        case 11:
            player->CLOSE_GOSSIP_MENU();
            if (player->IsInCombat())

                ChatHandler(player->GetSession()).PSendSysMessage("You can't use that right now!");
            else
                player->TeleportTo(0, 4301.491211, -2760.454834, 16.771429, 3.627491);
            
            break;
        }
    }
    void ApplyRandomMorph(Player* player)
    {
        uint32 random = (urand(1, 26)); // Change this line when adding more morphs
        {
            switch (random)
            {
            case 1: player->SetDisplayId(10134); break;     // Troll Female                 'Orb of Deception'
            case 2: player->SetDisplayId(10135); break;     // Troll Male                   'Orb of Deception'
            case 3: player->SetDisplayId(10136); break;     // Tauren Male                  'Orb of Deception'
            case 4: player->SetDisplayId(10137); break;     // Human Male                   'Orb of Deception'
            case 5: player->SetDisplayId(10138); break;     // Human Female                 'Orb of Deception'
            case 6: player->SetDisplayId(10139); break;     // Orc Male                     'Orb of Deception'
            case 7: player->SetDisplayId(10140); break;     // Orc Female                   'Orb of Deception' 
            case 8: player->SetDisplayId(10141); break;     // Dwarf Male                   'Orb of Deception'
            case 9: player->SetDisplayId(10142); break;     // Dwarf Female                 'Orb of Deception' 
            case 10: player->SetDisplayId(10143); break;    // NightElf Male                'Orb of Deception'
            case 11: player->SetDisplayId(10144); break;    // NightElf Female              'Orb of Deception'
            case 12: player->SetDisplayId(10145); break;    // Undead Female                'Orb of Deception'
            case 13: player->SetDisplayId(10146); break;    // Undead Male                  'Orb of Deception'
            case 14: player->SetDisplayId(10147); break;    // Tauren Female                'Orb of Deception'
            case 15: player->SetDisplayId(10148); break;    // Gnome Male                   'Orb of Deception'
            case 16: player->SetDisplayId(10149); break;    // Gnome Female                 'Orb of Deception'
            case 17: player->SetDisplayId(4527); break;     // Thrall                       'Orgrimmar Boss'
            case 18: player->SetDisplayId(11657); break;    // Lady Sylvanas                'Undercity Boss'
            case 19: player->SetDisplayId(4307); break;     // Cairne Bloodhoof             'Thunderbluff Boss'
            case 20: player->SetDisplayId(17122); break;    // Lor'themar Theron            'Silvermoon City Boss'
            case 21: player->SetDisplayId(3597); break;     // King Magni Bronzebeard       'Ironforge Boss'
            case 22: player->SetDisplayId(5566); break;     // Highlord Bolvar Fordragon    'Stormwind Boss'
            case 23: player->SetDisplayId(7006); break;     // High Tinker Mekkatorque      'Gnomer Boss'
            case 24: player->SetDisplayId(7274); break;     // Tyrande Whisperwind          'Darnassus Boss'
            case 25: player->SetDisplayId(21976); break;    // Arthus Small                 'Arthus'
            case 26: player->SetDisplayId(24641); break;    // Arthus Ghost                 'Arthus Ghost'

            default:
                break;
            }
        }
    }

    void SummonTempNPC(Player* player, uint32 entry, const char* salute = "")
    {
        if (!player || entry == 0)
            return;

        int npcDuration = sConfigMgr->GetIntDefault("Premium.NpcDuration", 60) * IN_MILLISECONDS;
        if (npcDuration <= 0) // Safeguard
            npcDuration = 60;

        Creature* npc = player->SummonCreature(entry, player->GetPositionX(), player->GetPositionY(), player->GetPositionZ(), 0, TEMPSUMMON_TIMED_DESPAWN_OUT_OF_COMBAT, npcDuration);
        npc->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NON_ATTACKABLE);
        npc->GetMotionMaster()->MoveFollow(player, PET_FOLLOW_DIST, player->GetFollowAngle());
        npc->setFaction(player->getFaction());

        if (salute && !(salute[0] == '\0'))
            npc->MonsterWhisper(salute, player, false);
    }

};

class PremiumCommands : public CommandScript
{
public:
    PremiumCommands() : CommandScript("PremiumCommands") {  }

    std::vector<ChatCommand> GetCommands() const override
    {
   
        static std::vector<ChatCommand> accountCommandTable
        {
            { "create",     SEC_GAMEMASTER, false, &HandlePremiumAccountCreateCommand, "" },
            { "delete",     SEC_GAMEMASTER, false, &HandlePremiumAccountDeleteCommand, "" },
            { "info",       SEC_GAMEMASTER, false, &HandlePremiumAccountInfoCommand, "" }
        };
        static std::vector<ChatCommand> commandTable =
        {
            { "account",    SEC_GAMEMASTER, false, nullptr, "", accountCommandTable},
           
           
        };
        static std::vector<ChatCommand> premiumCommandTable =
        {
            { "premium",    SEC_GAMEMASTER, false, nullptr, "", commandTable},
            { "home",     SEC_GAMEMASTER, false, &HandleHomeTeleportCommand, "" },
            { "DuelZone",       SEC_GAMEMASTER, false, &HandleDuelZoneCommand, "" }
        };
        return premiumCommandTable;
    }

    static bool HandleDuelZoneCommand(ChatHandler* handler, char const* /* args */) // unusued param args
    {
        Player* me = handler->GetSession()->GetPlayer();

        me->TeleportTo(0, 4301.491211,-2760.454834, 16.771429, 3.627491);
        return true;

    }

    static bool HandleHomeTeleportCommand(ChatHandler* handler, char const* /* args */) // unusued param args
    {

        Player* me = handler->GetSession()->GetPlayer();
        std::string home = sConfigMgr->GetStringDefault("QuickTeleport.homeLocation", "");
        bool enabled = sConfigMgr->GetBoolDefault("QuickTeleport.enabled", true);


     //   if (!me->GetSession()->IsPremium())
         //   return false;

        QueryResult result = WorldDatabase.PQuery("SELECT `map`, `position_x`, `position_y`, `position_z`, `orientation` FROM game_tele WHERE name = 'Dalaran'");

        if (!enabled)
            return false;

        if (!me)
            return false;

        if (me->IsInCombat())
            return false;

        if (!result)
            return false;

        do
        {
            Field* fields = result->Fetch();
            uint32 map = fields[0].GetUInt32();
            float position_x = fields[1].GetFloat();
            float position_y = fields[2].GetFloat();
            float position_z = fields[3].GetFloat();
            float orientation = fields[4].GetFloat();

            me->TeleportTo(map, position_x, position_y, position_z, orientation);
        } while (result->NextRow());
        return true;
    }



    static bool HandlePremiumAccountInfoCommand(ChatHandler* handler, const char* /*args*/)
    {
        Player* target = handler->getSelectedPlayer();
        std::string playerName;

         playerName = target->GetName().c_str();
        if (!target)
        {
            (ChatHandler(handler->GetSession())).PSendSysMessage(LANG_NO_CHAR_SELECTED);
            handler->SetSentErrorMessage(true);
            return false;
        }

        if (handler->HasLowerSecurity(target))
            return false;

        uint64 accountID = target->ToPlayer()->GetSession()->GetAccountId();



        int premium_level = GetAccountPremiumLevel(accountID);


        if (!premium_level)
        {
            (ChatHandler(handler->GetSession())).PSendSysMessage("%s's account doesn't have premium level.", playerName);
            handler->SetSentErrorMessage(true);
            return false;
        }
        else
            (ChatHandler(handler->GetSession())).PSendSysMessage("%s's account premium  level: %u ", playerName, premium_level);

        return true;
    }

    static bool HandlePremiumAccountCreateCommand(ChatHandler* handler, char const * args)
    {
        if (!*args)
            return false;

        // Which premium level to be added
        int premiumLevel = atoi((char*)args);

     

        Player* target = handler->getSelectedPlayer();
        std::string playerName;

         playerName = target->GetName().c_str();

        if (!target)
        {
            handler->SendSysMessage(LANG_NO_CHAR_SELECTED);
            handler->SetSentErrorMessage(true);
            return false;
        }

        if (handler->HasLowerSecurity(target))
            return false;

        uint64 accountID = target->ToPlayer()->GetSession()->GetAccountId();

        char* durationStr = strtok(nullptr, " ");
        if (!durationStr || !atoi(durationStr))
            return false;

        uint32 DurationSecs = TimeStringToSecs(durationStr);

        // Validate if account to be inserted already has a premium level
        int hasPremiumLevel = GetAccountPremiumLevel(accountID);

        if (!hasPremiumLevel)
        {

        ///    LoginDatabase.PQuery("INSERT INTO account_premium (id, premium_type) VALUES (%i, %i)", accountID, premiumLevel);
            LoginDatabase.PQuery("INSERT INTO account_premium VALUES(%i , UNIX_TIMESTAMP(), UNIX_TIMESTAMP() + %i , %i , 1)", accountID, DurationSecs, premiumLevel);
  /// 
            return true;
        }
        else

        {
            LoginDatabase.PQuery("UPDATE account_premium SET premium_type = %i, setdate=UNIX_TIMESTAMP(), unsetdate=UNIX_TIMESTAMP() + %i  WHERE id = %i", accountID, premiumLevel, DurationSecs);
            return true;
        }



        if (hasPremiumLevel)
            (ChatHandler(handler->GetSession())).PSendSysMessage("%s's account premium level set to: %u", playerName, premiumLevel);
        else
        {
            (ChatHandler(handler->GetSession())).PSendSysMessage("%s's account already has a premium level. Remove first.", playerName);
            handler->SetSentErrorMessage(true);
            return false;
        }

        return true;
    }

    static bool HandlePremiumAccountDeleteCommand(ChatHandler* handler, const char* /*args*/)
    {
        Player* target = handler->getSelectedPlayer();
        std::string playerName;

         playerName = target->GetName().c_str();
        if (!target)
        {
            (ChatHandler(handler->GetSession())).PSendSysMessage(LANG_NO_CHAR_SELECTED);
            handler->SetSentErrorMessage(true);
            return false;
        }

        if (handler->HasLowerSecurity(target))
            return false;

        uint64 accountID = target->ToPlayer()->GetSession()->GetAccountId();

        bool characterPremiumDeleted = DeleteAccountPremiumLevel(accountID);
        if (characterPremiumDeleted)
            (ChatHandler(handler->GetSession())).PSendSysMessage("%s's Premium account deleted", playerName);
        else
        {
            (ChatHandler(handler->GetSession())).PSendSysMessage("No premium level assigned to %s's account.", playerName);
            handler->SetSentErrorMessage(true);
            return false;
        }

        return true;
    }
    static int8 GetAccountPremiumLevel(uint64 accountID)
    {
        QueryResult result = LoginDatabase.PQuery("SELECT premium_type FROM account_premium WHERE id = %i", accountID);

        if (!result)
            return false;

        int8 account_premium_level = (*result)[0].GetInt8();

        return account_premium_level;
    }

    static bool CreateAccountPremiumLevel(uint64 accountID, int8 premiumLevel)
    {
        // Validate if account to be inserted already has a premium level
        int hasPremiumLevel = GetAccountPremiumLevel(accountID);

        if (!hasPremiumLevel)
        {
            LoginDatabase.PQuery("INSERT INTO account_premium (id, premium_type) VALUES (%i, %i)", accountID, premiumLevel);
            return true;
        }
        else

        {
            LoginDatabase.PQuery("UPDATE account_premium SET premium_type = %i WHERE id = %i", accountID, premiumLevel);
            return true;
        }
    }

    static bool DeleteAccountPremiumLevel(uint64 accountID)
    {
        // Validate if account to be removed has a premium level
        int hasPremiumLevel = GetAccountPremiumLevel(accountID);
        if (hasPremiumLevel)
        {
            LoginDatabase.PQuery("DELETE FROM account_premium WHERE id = %i", accountID);
            hasPremiumLevel = GetAccountPremiumLevel(accountID);
            if (!hasPremiumLevel)
                return true;

            return false;
        }
        else
            return false;
    }
};



void AddCharacterToolsScripts()
{
    new character_tools();
   
    new PremiumCommands();
  
}
